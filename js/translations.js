var translations =
   {
      "et": [
         {
            "where_does_dna"              :  "Kus peidab end DNA?",
            "human_body_dna"              :  "Inimese keha on DNAd täis,<br>kuid kus see lähemalt paikeneb?",            
            "find_out"                    :  "UURI JÄRELE!",
            "dna_packing"                 :  "DNA PAKKIMINE",
            "cell"                        :  "RAKK",
            "cell_nucleus"                :  "RAKUTUUM",
            "chromosome"                  :  "KROMOSOOM",
            "gene"                        :  "GEEN",
            "dna"                         :  "DNA",
            "cell_title"                  :  "Rakk",
            "cell_text"                   :  "Enamik inimese keharakke sisaldab pärilikkusainet, DNAd.<br>Erandiks on punased vererakud, mis küpsemisfaasis oma tuuma koos DNA-ga kaotavad.",
            "cell_nucleus_title"          :  "Rakutuum",
            "cell_nucleus_text"           :  "Rakutuum on raku osa, kus paikneb enamik raku DNAst (osa DNAst on ka mitkondris). Kõikides keha rakutuumades olev DNA on sarnase järjestusega, sest kõik inimese rakud on alguse saanud ühest viljastatud munarakust.",
            "chromosome_title"            :  "Kromosoom",
            "chromosome_text"             :  "Kromosoom on kokku pakitud osa DNAst, mida saab valgusmikroskoobiga vaadelda ühes kindlas raku jagunemisfaasis (metafaasis). Inimesel on 22×2 kromosoomi (kaks koopiat kromosoomi kohta) + 2 sugukromosoomi (naistel X ja X ning meestel X ja Y), milles esinev DNA moodustab kokku inimese genoomi.",
            "gene_title"                  :  "Geen",
            "gene_text"                   :  "Lihtsustatult on geen kindla eesmärgiga DNA ühik, mis pärandub vanematelt järglastele. Näiteks on inimesel umbes 20 000 geeni, mille alusel valmistatakse organismis eluks vajalikke valke. Samas moodustavad nende geenide piirkonnad ainult 5% kogu DNA järjestusest ja kaua aega arvati, et ülejäänud DNA-l mingit rolli ei olegi. Nüüdseks on siiski teada, et ka ülejäänud DNA-l on oma roll - näiteks reguleerida seda, kui palju mingis organis on vaja valku toota.",
            "dna_title"                   :  "DNA",
            "dna_text"                    :  "DNA on pärilikku infot kandev aine, mis koosneb nukleotiididest. Iga nukleotiid koosneb fosfaatrühmast, suhkur desoksüriboosist ja ühest neljast lämmastikalusest – adeniinist (A), tümiinist (T), tsütosiinist (C) või guaniinist (G).<br><br>Kaks lämmastikalust moodustavad omavahel paari - A paardub T-ga ja C paardub G-ga. Nukleotiidipaarid moodustavad DNA järjestuse, mille pikkus on inimesel umbes 3,2 miljardit ja mis on igal inimesel individuaalne (v.a ühemunakaksikud, kellel on DNA järjestus peaaegu identne).",
         }
      ],
      "en": [
         {
            "where_does_dna"              :  "Where does DNA hide itself?",
            "human_body_dna"              :  "The human body is full of DNA,<br>but where is it specifically located?",            
            "find_out"                    :  "FIND OUT!",
            "dna_packing"                 :  "DNA PACKAGING",
            "cell"                        :  "CELL",
            "cell_nucleus"                :  "CELL NUCLEUS",
            "chromosome"                  :  "CHROMOSOME",
            "gene"                        :  "GENE",
            "dna"                         :  "DNA",
            "cell_title"                  :  "Cell",
            "cell_text"                   :  "Most of the cells in the human body contain genetic material or DNA.<br>Red blood cells, which lose their nucleus together with DNA during maturation, are an exception.",
            "cell_nucleus_title"          :  "Cell nucleus",
            "cell_nucleus_text"           :  "Cell nucleus is the part of the cell where most of its DNA is located (DNA can also be found in the mitochondrion). The DNA in every cell nucleus has a similar sequence, because all human cells originate from the same single fertilised egg cell.",
            "chromosome_title"            :  "Chromosome",
            "chromosome_text"             :  "A chromosome is a packaged part of DNA that can only be seen under a light microscope during one particular cell division phase (metaphase). Humans have 22 × 2 chromosomes (two copies per chromosome) + 2 sex chromosomes (X and X for women and X and Y for men); the DNA in these chromosomes forms the human genome.",
            "gene_title"                  :  "Gene",
            "gene_text"                   :  "To put it simply, a gene is a DNA unit with a specific function that is passed from parents to children. For example, one person has around 20,000 genes that produce proteins necessary for the life of a body. At the same time, the regions of such genes form only about 5% of the entire DNA sequence, and for a long time it was thought that the remaining DNA had no role to play. However, we now know that the rest of the DNA does have a function, such as regulating the amount of protein that should be produced in an organ.",
            "dna_title"                   :  "DNA",
            "dna_text"                    :  "DNA is a substance made of nucleotides that carries heredity information. Each nucleotide is composed of a phosphate group, a sugar deoxyribose and one of four nitrogenous bases—adenine (A), thymine (T), cytosine (C) or guanine (G).<br><br>Two nitrogenous bases form pairs—A is always paired with T, and C with G. Nucleotide pairs form the DNA sequence, which in humans consists of nearly 3.2 billion pairs and which is unique in each human (except identical twins, whose DNA sequence is almost identical).",

         }
      ],
      "ru": [
         {
            "where_does_dna"              :  "Где прячется ДНК?",
            "human_body_dna"              :  "Организм человека наполнен ДНК,<br>но где именно она находится?",            
            "find_out"                    :  "ВЫЯСНИТЕ!",
            "dna_packing"                 :  "УПАКОВКА ДНК",
            "cell"                        :  "КЛЕТКА",
            "cell_nucleus"                :  "ЯДРО КЛЕТКИ",
            "chromosome"                  :  "ХРОМОСОМА",
            "gene"                        :  "ГЕН",
            "dna"                         :  "ДНК",
            "cell_title"                  :  "Клетка",
            "cell_text"                   :  "Большинство клеток человека содержат наследственное вещество - ДНК.<br>Исключением являются эритроциты, которые на фазе созревания теряют свое ядро вместе с ДНК. ",
            "cell_nucleus_title"          :  "Ядро клетки",
            "cell_nucleus_text"           :  "Ядро клетки - часть клетки, в которой расположена большая часть ДНК клетки (часть ДНК содержится также в митохондрии). Во всех клеточных ядрах организма ДНК имеет одинаковую последовательность, поскольку все клетки человека берут начало из одной оплодотворенной яйцеклетки.",
            "chromosome_title"            :  "Хромосома",
            "chromosome_text"             :  "Хромосома – упакованная часть ДНК, которую можно рассмотреть с помощью светового микроскопа на определенной стадии деления клетки (метафаза). У человека 22 × 2 хромосомы (по две копии каждой хромосомы) + 2 половые хромосомы (у женщин X и X, у мужчин X и Y), в которых присутствующая ДНК образует геном человека.",
            "gene_title"                  :  "Ген",
            "gene_text"                   :  "Упрощенно ген - это выполняющий определенную цель участок ДНК, передающийся от родителей потомству. Например, у человека имеется примерно 20 000 генов, на основании которых в организме производятся необходимые для жизни белки. В то же время, участки этих генов составляют только 5% всей последовательности ДНК, и долгое время считалось, что остальная часть ДНК никаой задачи не выполняет. Однако сейчас известно, что и остальная часть ДНК играет свою роль - например, регулирует, сколько в каком органе нужно производить белка.",
            "dna_title"                   :  "ДНК",
            "dna_text"                    :  "ДНК - вещество, несущее наследственную информацию и состоящее из нуклеотидов. Каждый нуклеотид состоит из фосфатной группы, сахара - дезоксирибозы и одного из четырех азотистых оснований – аденина (А), тимина (Т), цитозина (С) или гуанина (G).<br><br>Два азотистых основания образуют пару – А в паре с Т, С в паре с G. Нуклеотидные пары образуют последовательность ДНК, длина которой у человека составляет примерно 3,2 миллиарда и которая для каждого человека индивидуальна (кроме однояйцевых близнецов, у которых последовательность ДНК идентична).",

         }
      ],
      "fi": [
         {
            "where_does_dna"              :  "Missä DNA piileksii?",
            "human_body_dna"              :  "Ihmisen keho on täynnä DNA:ta,<br>mutta missä se tarkemmin ottaen sijaitsee?",            
            "find_out"                    :  "OTA SELVÄÄ!",
            "dna_packing"                 :  "DNA:n pakkaaminen",
            "cell"                        :  "SOLU",
            "cell_nucleus"                :  "TUMA",
            "chromosome"                  :  "KROMOSOMI",
            "gene"                        :  "GEENI",
            "dna"                         :  "DNA",
            "cell_title"                  :  "Solu",
            "cell_text"                   :  "Enemmistö ihmiskehon soluista sisältää perinnöllisyysainetta, DNA:ta.<br>Poikkeuksia ovat punaiset verisolut, jotka kadottavat kypsymisvaiheessa tumansa DNA:n kanssa. ",
            "cell_nucleus_title"          :  "Tuma",
            "cell_nucleus_text"           :  "Tuma on solun osa, jossa sijaitsee valtaosa solun DNA:sta (osa DNA:sta on myös mitokondriossa). Kaikissa kehon solujen tumissa oleva DNA on samanlaisessa järjestyksessä, sillä kaikki ihmisen solut ovat saaneet alkunsa yhdestä hedelmöittyneestä munasolusta.",
            "chromosome_title"            :  "Kromosomi",
            "chromosome_text"             :  "Kromosomi on DNAn kokoonpuristunut osa, jota voi tarkastella proteiinimikroskoopilla yhdessä varmassa solun jakautumisjaksossa (metafaasissa). Ihmisellä on 22 X 2 kromosomia (kaksi kopiota kromosomia kohti) + 2 sukupuolikromosomia (naisilla X ja X ja miehillä X ja Y), joissa sijaitseva DNA muodostaa ihmisen geeniperimän.",
            "gene_title"                  :  "Geeni",
            "gene_text"                   :  "Yksinkertaistaen voidaan sanoa, että geeni on DNA:n tärkeä yksikkö, joka periytyy vanhemmilta jälkeläisille. Ihmisellä on esimerkiksi 20 000 geeniä, joilla elimistössä valmistetaan elämälle tarpeellisia proteiineja. Samalla näiden geenien alueet muodostavat vain 5 % koko DNA:n järjestyksestä, ja pitkään luultiin, että muulla DNA:lla ei olekaan mitään tehtävää. Nykyään kuitenkin tiedetään, että myös muulla DNA:lla on oma tehtävänsä - se esimerkiksi säätelee sitä, paljonko jossakin elimessä on tarpeen tuottaa proteiinia.",
            "dna_title"                   :  "DNA",
            "dna_text"                    :  "DNA – perinnöllisen tiedon sisältävä aine, joka koostuu nukleotideista. Jokainen nukleotidi koostuu fosfaattiryhmästä, sokeri deoksiribosuksesta ja yhdestä seuraavista neljästä typpiemäksestä – adeniinista (A), tymiinistä (T), sytosiinista (C) tai guaniinista (G). Kaksi typpiemästä muodostaa keskenään parin – A on parina T:lle ja C on parina G:lle.<br><br>ZNukleotidiparit muodostavat DNA-sarjan, jonka pituus on ihmisellä noin 3,2 miljardia ja joka on jokaisella henkilöllä yksilöllinen (poikkeuksena samanmunaiset kaksoset, joilla DNA-sarja on lähes identtinen).",

         }
      ]
   }